import numpy
import numpy as np
import scipy
import sys

def sim_NARX_Photoreceptor_Zeros(U,theta):   

    
    N = len(U)
    NARX_input = numpy.zeros(np.size(U))
    y          = numpy.zeros(np.size(U))
       
    #for k in range(MaxLag+1,N-1):
    for k in xrange(N-1):
                
        #Data scalation
        if(U[k]<=10.0**-10.0):
            U[k]=10.0**-10.0
        
        if   k==0:                        
            

            NARX_input[k] = U[k]
            
            #NARX
            y[k]=0
            y_max = y[k]
            y_min = y[k]
                
        elif k >= 1:
            
            
            NARX_input[k] = U[k]
            
            
            if k==1:                
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]
        
            elif k==2:                
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]
        
            elif k==3:
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]+theta[2-1]*y[k-03]
        
            elif k==4:
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]+theta[2-1]*y[k-03]+theta[9-1]*y[k-04]+theta[12-1]*NARX_input[k-04]*y[k-02]+theta[15-1]*NARX_input[k-04] 
        
            elif k==5:
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]+theta[2-1]*y[k-03]+theta[9-1]*y[k-04]+theta[12-1]*NARX_input[k-04]*y[k-02]+theta[15-1]*NARX_input[k-04]+theta[3-1]*NARX_input[k-05]*NARX_input[k-04]+theta[10-1]*y[k-05]+theta[11-1]*NARX_input[k-04]*y[k-05]+theta[14-1]*NARX_input[k-05]
                
            elif k==6:
                #NARX
                y[k]= 0#theta[4-1] + theta[1-1]*y[k-01]+theta[2-1]*y[k-03]+theta[9-1]*y[k-04]+theta[12-1]*NARX_input[k-04]*y[k-02]+theta[15-1]*NARX_input[k-04]+theta[3-1]*NARX_input[k-05]*NARX_input[k-04]+theta[10-1]*y[k-05]+theta[11-1]*NARX_input[k-04]*y[k-05]+theta[14-1]*NARX_input[k-05]+theta[5-1]*NARX_input[k-06]+theta[6-1]*NARX_input[k-04]*y[k-06]
        
            elif k>=7:
                y[k]=theta[1-1]*y[k-01]+theta[2-1]*y[k-03]+theta[3-1]*NARX_input[k-05]*NARX_input[k-04]+theta[4-1]+theta[5-1]*NARX_input[k-06]+theta[6-1]*NARX_input[k-04]*y[k-06]+theta[7-1]*NARX_input[k-07]+ theta[8-1]*NARX_input[k-07]*NARX_input[k-06]+ theta[9-1]*y[k-04]+ theta[10-1]*y[k-05]+ theta[11-1]*NARX_input[k-04]*y[k-05]+ theta[12-1]*NARX_input[k-04]*y[k-02]+ theta[13-1]*NARX_input[k-07]*NARX_input[k-03]+ theta[14-1]*NARX_input[k-05] + theta[15-1]*NARX_input[k-04]           
            
                
            #Get max val
            if y[k] > y_max :
                y_max = y[k]
            
            #Get max val
            if y[k] < y_min :
                y_min = y[k]

    return (y,y_min,y_max)

def simNarxPhotoreceptor(theta,NARX_input):
    """
    The strucure, which is fix, is the following:
            y[k-1] = theta[1-1]*y[k-01-1]+
                 theta[2-1]*y[k-03-1]+
                 theta[3-1]*NARX_input[k-05-1]*NARX_input[k-04-1]+
                 theta[4-1]+
                 theta[5-1]*NARX_input[k-06-1]+
                 theta[6-1]*NARX_input[k-04-1]*y[k-06-1]+
                 theta[7-1]*NARX_input[k-07-1]+
                 theta[8-1]*NARX_input[k-07-1]*NARX_input[k-06-1]+
                 theta[9-1]*y[k-04-1]+
                 theta[10-1]*y[k-05-1]+
                 theta[11-1]*NARX_input[k-04-1]*y[k-05-1]+
                 theta[12-1]*NARX_input[k-04-1]*y[k-02-1]+
                 theta[13-1]*NARX_input[k-07-1]*NARX_input[k-03-1]+
                 theta[14-1]*NARX_input[k-05-1]+
                 theta[15-1]*NARX_input[k-04-1]    
    """
    N=np.size(NARX_input)              #number of data samples
    MaxLag=7                        #maximum number of lags in the model
    y=numpy.zeros(np.size(NARX_input))
    for k in range(MaxLag+1,N-1):
        y[k-1]=theta[1-1]*y[k-01-1]+theta[2-1]*y[k-03-1]+theta[3-1]*NARX_input[k-05-1]*NARX_input[k-04-1]+theta[4-1]+theta[5-1]*NARX_input[k-06-1]+theta[6-1]*NARX_input[k-04-1]*y[k-06-1]+theta[7-1]*NARX_input[k-07-1]+ theta[8-1]*NARX_input[k-07-1]*NARX_input[k-06-1]+ theta[9-1]*y[k-04-1]+ theta[10-1]*y[k-05-1]+ theta[11-1]*NARX_input[k-04-1]*y[k-05-1]+ theta[12-1]*NARX_input[k-04-1]*y[k-02-1]+ theta[13-1]*NARX_input[k-07-1]*NARX_input[k-03-1]+ theta[14-1]*NARX_input[k-05-1]    + theta[15-1]*NARX_input[k-04-1]    
    return y

